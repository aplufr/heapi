#!/bin/bash
cd "$(dirname -- "${0}")" || exit 1
# as we must be root, if not switch to DEBUG mode
[ $UID -ne 0 ] && DEBUG=${DEBUG:-1}
source libheapi
usage ()
{
	echo "--- usage ---"
	echo "$0 : a control heat system for Pi"
	echo "$0 init"
	echo " -> will enable all output + set eco mode"
	echo "$0 list"
	echo " -> list all card available"
	echo "$0 cuisine [conf|eco|horsgel|stop]"
	echo " -> set card cuisine in choosen mode"
	echo "card must be defined in cards"
	exit "${1:-3}"
}
if [ $# -lt 1 ]
then
	usage
fi
if [ $# -eq 1 ]
then
	case ${1} in
		init)
			init
			;;
		list)
			list
			;;
		*)
			usage
			;;
	esac
	exit
fi
if [ $# -eq 2 ]
then
	if card"${1^}" 2>/dev/null
	then
		if ! ${2} 2>/dev/null
		then
			echo "Can't set/get mode! Wrong card? No init? or bad command."
			echo "card: ${1^}"
			echo "command: ${2}"
			usage 2
		fi
	else
		echo "No valid control card"
		usage 1
	fi
else
	usage
fi

