# heapi
HeaPi, a way to control electric heater with a raspberry Pi

This use the pilot wire to set the mode on your electrical convector/heater.

# Install

1. Copy all files into /root/heapi

2. Edit cards to matchs cards connexions

3. Set-up crontab as this:

   ln -s /root/heapi/crontab /etc/cron.d/heapi

4. Edit your schedule within crontab file (see man 5 crontab for syntax)

# Cards and connexions

See docs for details

# Thanks

If you want, you may bring me an apple pie if we ever met (but tea is fine too).

# References

gpio source came from: https://github.com/lasandell/RaspberryPi/

