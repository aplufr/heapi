Connection RaspberryPi 2 with 4 control cards
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Physical and logical view
+++++++++++++++++++++++++++++++++++

+------+-------+------+------+--------+
|CARDS | LOCAL |COLOR |PIN PI|PIN SOFT|
+======+=======+======+======+========+
|1     |SEJOUR1|YELLOW|10    |15      |
+      |FENETRE+------+------+--------+
|      |       |GREEN |12    |18      |
+------+-------+------+------+--------+
|2     |CHAMBRE|YELLOW|13    |27      |
+      |       +------+------+--------+
|      |       |GREEN |11    |17      |
+------+-------+------+------+--------+
|3     |SEJOUR2|YELLOW| 3    | 2      |
+      |ETAGERE+------+------+--------+
|      |       |GREEN | 5    | 3      |
+------+-------+------+------+--------+
|4     |CUISINE|YELLOW| 8    |14      |
+      |       +------+------+--------+
|      |       |GREEN | 7    | 4      |
+------+-------+------+------+--------+


For details, please refer to: https://pinout.xyz/

* PIN PI: is the physical pin on the board.
* PIN SOFT: is the logical pin BCM #
* COLOR: is used to identify the cable (alpha version used wire yellow/green). 


Output mode
+++++++++++

+--------+------+-----+
|MODE    |YELLOW|GREEN|
+========+======+=====+
|CONF    |0     |0    |
+--------+------+-----+
|ECO     |1     |1    |
+--------+------+-----+
|Frost   |1     |0    |
+--------+------+-----+
|Stop    |0     |1    |
+--------+------+-----+

Frost mode is supposed to maintain about 7°C while stop do what it say.


Physical view
++++++++++++++

::
  
              +---------+
              |Raspberry|
              |   Pi    |
              +---------+
     
     +-----+-----+-----+-----+-----+
     |  E  |     |     |     |     |
     |  N  |  4  |  3  |  2  |  1  |
     |  D  |     |     |     |     |
     +-----+-----+-----+-----+-----+
   


